import java.util.Scanner;

public class Main {

    public static boolean palindrome(String word)
    {
        StringBuffer str = new StringBuffer();
        str.append(word);
        StringBuffer reverseStr = new StringBuffer();
        reverseStr = str.reverse();
        reverseStr.toString();
        str.toString();
        if(reverseStr.equals(str))
            return true;
        else return false;
    }
    public static int numOfDifferLet(String word) //ф-ция считает количество различных символов
    {
        StringBuffer symbols = new StringBuffer();
        String symb;
        for(int i = 0; i<word.length()-1; i++)
        {
            symb = String.valueOf(word.charAt(i));
            if (symbols.indexOf(symb)==-1)
                symbols.append(symb);
        }
        return symbols.length();
    }
    public static boolean isLatinLet(String str)
    {
        return str.matches("[a-zA-Z]+");
    }


    public static void main(String[] args)
    {
        StringBuffer enteredStrB = new StringBuffer();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String enteredStr = scanner.nextLine();
        enteredStrB.append(enteredStr);
        String strOOP = "object-oriented programming";
        String reserve;

        System.out.println("Строка до замены: ");
        System.out.println(enteredStrB);

        //замена на ООР
        int amountEntry = 0;
        System.out.println("\nСтрока после замены на ООР: ");
        for (int i=0; i<(enteredStrB.length()-strOOP.length()); i++)
        {
            if(enteredStr.regionMatches(true,i, strOOP, 0, strOOP.length()-1))
            {
                amountEntry++;
                if(amountEntry%2==0)
                {
                    enteredStrB.replace(i, i+strOOP.length(), "OOP");
                }
            }
        }
        System.out.println(enteredStrB);

        // поиск слова наименьшим количеством различных символов
        int latinLet = 0;
        String[] splitStr = enteredStr.split(" ");
        int[] numOfDifLet = new int[splitStr.length];
        for(int i = 0; i<splitStr.length; i++)
        {
            numOfDifLet[i]=numOfDifferLet(splitStr[i]);
            if(isLatinLet(splitStr[i])) // подсчёт слов из латинских букв
                latinLet++;
        }
        int minNumLet = numOfDifLet[0], numOfWord = 0;
        for(int i = 0; i<numOfDifLet.length; i++)
        {
            if(numOfDifLet[i]<minNumLet)
            {
                minNumLet = numOfDifLet[i];
                numOfWord = i;
            }
        }
        System.out.println("\nСлово с наименьшим числом различных символов: " + splitStr[numOfWord]);
        System.out.println("\nКоличество слов из латинских букв: " + latinLet);

        // поиск слов палиндромов
        System.out.println("\nСлова палиндромы: ");
        for(int i = 0; i<splitStr.length; i++)
        {
            if (palindrome(splitStr[i]))
            System.out.print(splitStr[i]+" ");
        }
    }
}